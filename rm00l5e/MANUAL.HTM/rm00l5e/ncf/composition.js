var maptitle = new Array("New Car Features"); //NCF

var tree = new Array(
"Second Hierarchy","Second HierarchyKey","Third Hierarchy","Third HierarchyKey","Fourth Hierarchy","PDF Files Name",
"NEW MODEL OUTLINE","1","EXTERIOR APPEARANCE","1","SINGLE CAB","m_mo_0002.pdf",
"","1","","1","DOUBLE CAB","m_mo_0003.pdf",
"","1","","1","EXTRA CAB","m_mo_0004.pdf",
"","1","","1","SMART CAB","m_mo_0005.pdf",
"","1","","1","MODEL CODE","m_mo_0006.pdf",
"","1","","1","MODEL LINE-UP","m_mo_0007.pdf",
"","1","EXTERIOR","2","Front View","m_mo_0013.pdf",
"","1","","2","Rear View","m_mo_0014.pdf",
"","1","","2","Side View","m_mo_0015.pdf",
"","1","","2","Over Fender","m_mo_0016.pdf",
"","1","","2","Side Step","m_mo_0016.pdf",
"","1","","2","Tire &#38; Disc Wheel","m_mo_0017.pdf",
"","1","","2","Color List","m_mo_0018.pdf",
"","1","INTERIOR","3","Instrument Panel","m_mo_0019.pdf",
"","1","","3","Center Cluster","m_mo_0019.pdf",
"","1","","3","Combination Meter","m_mo_0020.pdf",
"","1","","3","Shift Lever (Automatic Transmission)","m_mo_0020.pdf",
"","1","","3","Steering Wheel","m_mo_0021.pdf",
"","1","","3","Rear Seat (Double Cab Model)","m_mo_0022.pdf",
"","1","EQUIPMENT","4","Audio System","m_mo_0023.pdf",
"","1","","4","Blocking System","m_mo_0024.pdf",
"","1","PERFORMANCE","5","Power Train","m_mo_0025.pdf",
"","1","","5","Chassis","m_mo_0028.pdf",
"","1","ENVIRONMENT and RECYCLING","6","Use of TSOP &#38; PP","m_mo_0029.pdf",
"","1","","6","Use of Lead-free Parts","m_mo_0029.pdf",
"","1","DIMENSIONS","7","DIMENSIONS","m_mo_0030.pdf",
"ENGINE","2","1TR-FE AND 2TR-FE ENGINES","8","DESCRIPTION","m_eg_0002.pdf",
"","2","","8","MAJOR DIFFERENCE","m_eg_0005.pdf",
"","2","","8","FEATURES OF 1TR-FE AND 2TR-FE ENGINES","m_eg_0006.pdf",
"","2","","8","ENGINE PROPER","m_eg_0007.pdf",
"","2","","8","VALVE MECHANISM","m_eg_0014.pdf",
"","2","","8","LUBRICATION SYSTEM","m_eg_0017.pdf",
"","2","","8","COOLING SYSTEM","m_eg_0020.pdf",
"","2","","8","INTAKE AND EXHAUST SYSTEM","m_eg_0022.pdf",
"","2","","8","FUEL SYSTEM","m_eg_0025.pdf",
"","2","","8","IGNITION SYSTEM","m_eg_0026.pdf",
"","2","","8","CHARGING SYSTEM","m_eg_0028.pdf",
"","2","","8","STARTING SYSTEM","m_eg_0029.pdf",
"","2","","8","SERPENTINE BELT DRIVE SYSTEM","m_eg_0032.pdf",
"","2","","8","ENGINE CONTROL SYSTEM","m_eg_0033.pdf",
"","2","2TR-FBE ENGINE","9","DESCRIPTION","m_eg_0065.pdf",
"","2","","9","MAJOR DIFFERENCE (from 2TR-FE Engine)","m_eg_0066.pdf",
"","2","","9","ENGINE PROPER","m_eg_0067.pdf",
"","2","","9","VALVE MECHANISM","m_eg_0068.pdf",
"","2","","9","INTAKE AND EXHAUST SYSTEM","m_eg_0068.pdf",
"","2","","9","FUEL SYSTEM","m_eg_0069.pdf",
"","2","","9","IGNITION SYSTEM","m_eg_0073.pdf",
"","2","","9","ENGINE CONTROL SYSTEM","m_eg_0074.pdf",
"","2","1GR-FE ENGINE","10","DESCRIPTION","m_eg_0085.pdf",
"","2","","10","FEATURES OF 1GR-FE ENGINE","m_eg_0088.pdf",
"","2","","10","ENGINE PROPER","m_eg_0089.pdf",
"","2","","10","VALVE MECHANISM","m_eg_0096.pdf",
"","2","","10","LUBRICATION SYSTEM","m_eg_0099.pdf",
"","2","","10","COOLING SYSTEM","m_eg_0102.pdf",
"","2","","10","INTAKE AND EXHAUST SYSTEM","m_eg_0105.pdf",
"","2","","10","FUEL SYSTEM","m_eg_0110.pdf",
"","2","","10","IGNITION SYSTEM","m_eg_0112.pdf",
"","2","","10","CHARGING SYSTEM","m_eg_0114.pdf",
"","2","","10","STARTING SYSTEM","m_eg_0114.pdf",
"","2","","10","SERPENTINE BELT DRIVE SYSTEM","m_eg_0114.pdf",
"","2","","10","ENGINE CONTROL SYSTEM","m_eg_0115.pdf",
"","2","5L-E ENGINE","11","DESCRIPTION","m_eg_0135.pdf",
"","2","","11","FEATURES OF 5L-E ENGINE","m_eg_0137.pdf",
"","2","","11","ENGINE PROPER","m_eg_0138.pdf",
"","2","","11","VALVE MECHANISM","m_eg_0142.pdf",
"","2","","11","LUBRICATION SYSTEM","m_eg_0144.pdf",
"","2","","11","COOLING SYSTEM","m_eg_0146.pdf",
"","2","","11","INTAKE AND EXHAUST SYSTEM","m_eg_0148.pdf",
"","2","","11","FUEL SYSTEM","m_eg_0150.pdf",
"","2","","11","ENGINE CONTROL SYSTEM","m_eg_0153.pdf",
"","2","1KD-FTV AND 2KD-FTV ENGINES","12","DESCRIPTION","m_eg_0168.pdf",
"","2","","12","MAJOR DIFFERENCE","m_eg_0173.pdf",
"","2","","12","FEATURES OF 1KD-FTV AND 2KD-FTV ENGINES","m_eg_0174.pdf",
"","2","","12","ENGINE PROPER","m_eg_0176.pdf",
"","2","","12","VALVE MECHANISM ","m_eg_0181.pdf",
"","2","","12","LUBRICATION SYSTEM","m_eg_0184.pdf",
"","2","","12","COOLING SYSTEM","m_eg_0187.pdf",
"","2","","12","INTAKE AND EXHAUST SYSTEM","m_eg_0189.pdf",
"","2","","12","FUEL SYSTEM","m_eg_0201.pdf",
"","2","","12","CHARGING SYSTEM","m_eg_0211.pdf",
"","2","","12","STARTING SYSTEM","m_eg_0212.pdf",
"","2","","12","SERPENTINE BELT DRIVE SYSTEM","m_eg_0213.pdf",
"","2","","12","ENGINE CONTROL SYSTEM","m_eg_0214.pdf",
"CHASSIS","3","CLUTCH","13","DESCRIPTION","m_ch_0002.pdf",
"","3","G50&#8218; G55&#8218; G58 AND G58F MANUAL TRANSMISSIONS","14","DESCRIPTION","m_ch_0006.pdf",
"","3","","14","REVERSE SYNCHROMESH MECHANISM","m_ch_0008.pdf",
"","3","R150&#8218; R150F&#8218; R151 AND R151F MANUAL TRANSMISSIONS","15","DESCRIPTION","m_ch_0010.pdf",
"","3","A340E&#8218; A340F&#8218; A343E AND A343F AUTOMATIC TRANSMISSIONS","16","DESCRIPTION","m_ch_0013.pdf",
"","3","","16","TORQUE CONVERTER","m_ch_0017.pdf",
"","3","","16","OIL PUMP","m_ch_0017.pdf",
"","3","","16","PLANETARY GEAR UNIT","m_ch_0018.pdf",
"","3","","16","VALVE BODY UNIT","m_ch_0023.pdf",
"","3","","16","ELECTRONIC CONTROL SYSTEM","m_ch_0024.pdf",
"","3","","16","SHIFT CONTROL MECHANISM","m_ch_0034.pdf",
"","3","A750E AND A750F AUTOMATIC TRANSMISSIONS","17","DESCRIPTION","m_ch_0037.pdf",
"","3","","17","ATF (AUTOMATIC TRANSMISSION FLUID) ","m_ch_0040.pdf",
"","3","","17","TORQUE CONVERTER","m_ch_0041.pdf",
"","3","","17","OIL PUMP","m_ch_0041.pdf",
"","3","","17","PLANETARY GEAR UNIT","m_ch_0042.pdf",
"","3","","17","VALVE BODY UNIT","m_ch_0050.pdf",
"","3","","17","ELECTRONIC CONTROL SYSTEM","m_ch_0054.pdf",
"","3","","17","SHIFT CONTROL MECHANISM","m_ch_0070.pdf",
"","3","TRANSFER","18","DESCRIPTION","m_ch_0071.pdf",
"","3","","18","PLANETARY GEAR","m_ch_0072.pdf",
"","3","","18","4WD SYSTEM","m_ch_0074.pdf",
"","3","PROPELLER SHAFT","19","DESCRIPTION","m_ch_0079.pdf",
"","3","FRONT DRIVE SHAFT","20","DESCRIPTION","m_ch_0081.pdf",
"","3","DIFFERENTIAL","21","DESCRIPTION","m_ch_0082.pdf",
"","3","","21","REAR DIFFERENTIAL LOCK SYSTEM","m_ch_0089.pdf",
"","3","SUSPENSION AND AXLE","22","SUSPENSION","m_ch_0094.pdf",
"","3","","22","AXLE","m_ch_0096.pdf",
"","3","BRAKE","23","DESCRIPTION","m_ch_0097.pdf",
"","3","","23","BRAKE CONTROL SYSTEM (ABS)","m_ch_0109.pdf",
"","3","","23","BRAKE CONTROL SYSTEM (ABS&#8218; EBD&#8218; BRAKE ASSIST&#8218; TRC and VSC)","m_ch_0115.pdf",
"","3","STEERING","24","DESCRIPTION","m_ch_0142.pdf",
"","3","","24","STEERING GEAR","m_ch_0143.pdf",
"","3","","24","VANE PUMP","m_ch_0144.pdf",
"","3","","24","TILT MECHANISM","m_ch_0145.pdf",
"","3","","24","ENERGY ABSORBING MECHANISM","m_ch_0146.pdf",
"BODY","4","BODY STRUCTURE","25","LIGHTWEIGHT AND HIGHLY RIGID BODY","m_bo_0002.pdf",
"","4","","25","SAFETY FEATURES","m_bo_0002.pdf",
"","4","","25","RUST-RESISTANT BODY","m_bo_0005.pdf",
"","4","","25","LOW VIBRATION AND LOW NOISE BODY","m_bo_0007.pdf",
"","4","ENHANCEMENT OF PRODUCT APPEAL","26","SEAT BELT","m_bo_0012.pdf",
"BODY ELECTRICAL","5","LIGHTING","27","HEADLIGHT","m_be_0002.pdf",
"","5","","27","MANUAL HEADLIGHT BEAM LEVEL CONTROL SYSTEM","m_be_0003.pdf",
"","5","","27","LIGHT REMINDER SYSTEM ","m_be_0003.pdf",
"","5","","27","DAYTIME RUNNING LIGHT SYSTEM","m_be_0004.pdf",
"","5","","27","ILLUMINATED ENTRY SYSTEM","m_be_0005.pdf",
"","5","","27","HEADLIGHT CLEANER SYSTEM","m_be_0008.pdf",
"","5","METER","28","COMBINATION METER","m_be_0010.pdf",
"","5","COMBINATION SWITCH","29","DESCRIPTION","m_be_0019.pdf",
"","5","AIR CONDITIONING","30","DESCRIPTION","m_be_0020.pdf",
"","5","","30","SYSTEM DIAGRAM","m_be_0022.pdf",
"","5","","30","LAYOUT OF MAIN COMPONENTS","m_be_0025.pdf",
"","5","","30","REFRIGERATION CYCLE","m_be_0027.pdf",
"","5","","30","MODE POSITION AND DAMPER OPERATION","m_be_0029.pdf",
"","5","","30","AIR OUTLETS AND AIR FLOW VOLUME","m_be_0032.pdf",
"","5","","30","CONSTRUCTION","m_be_0034.pdf",
"","5","","30","AIR CONDITIONING CONTROL","m_be_0043.pdf",
"","5","","30","VISCOUS TYPE POWER HEATER","m_be_0046.pdf",
"","5","MULTI-INFORMATION DISPLAY","31","DESCRIPTION","m_be_0048.pdf",
"","5","","31","FUNCTION OF MAIN COMPONENTS","m_be_0049.pdf",
"","5","POWER WINDOW SYSTEM","32","DESCRIPTION","m_be_0051.pdf",
"","5","","32","LAYOUT OF MAIN COMPONENTS","m_be_0053.pdf",
"","5","","32","JAM PROTECTION FUNCTION","m_be_0054.pdf",
"","5","DOOR LOCK CONTROL SYSTEM","33","DESCRIPTION","m_be_0056.pdf",
"","5","","33","LAYOUT OF MAIN COMPONENTS","m_be_0057.pdf",
"","5","WIRELESS DOOR LOCK CONTROL SYSTEM","34","DESCRIPTION","m_be_0058.pdf",
"","5","","34","LAYOUT OF MAIN COMPONENTS","m_be_0060.pdf",
"","5","","34","FUNCTION","m_be_0061.pdf",
"","5","ENGINE IMMOBILISER SYSTEM","35","DESCRIPTION","m_be_0063.pdf",
"","5","","35","LAYOUT OF MAIN COMPONENTS","m_be_0063.pdf",
"","5","THEFT DETERRENT SYSTEM","36","DESCRIPTION","m_be_0064.pdf",
"","5","","36","LAYOUT OF MAIN COMPONENTS","m_be_0065.pdf",
"","5","","36","SYSTEM OPERATION","m_be_0066.pdf",
"","5","SRS AIRBAG SYSTEM","37","DESCRIPTION","m_be_0069.pdf",
"","5","","37","LAYOUT OF MAIN COMPONENTS","m_be_0071.pdf",
"","5","","37","AIRBAG FOR FRONTAL COLLISION","m_be_0072.pdf",
"","5","","37","AIRBAG FOR SIDE/REAR SIDE COLLISION","m_be_0073.pdf",
"","5","","37","DIAGNOSIS","m_be_0075.pdf",
"","5","SEAT BELT WARNING SYSTEM","38","DESCRIPTION","m_be_0076.pdf",
"","5","","38","OCCUPANT DETECTION SENSOR","m_be_0077.pdf",
"","5","CRUISE CONTROL SYSTEM","39","DESCRIPTION","m_be_0078.pdf",
"","5","","39","LAYOUT OF MAIN COMPONENTS","m_be_0079.pdf",
"","5","","39","FUNCTION","m_be_0080.pdf",
"","5","","39","DIAGNOSIS","m_be_0081.pdf",
"","5","POWER SEAT SYSTEM","40","DESCRIPTION","m_be_0082.pdf",
"","5","SEAT HEATER","41","DESCRIPTION","m_be_0083.pdf",
"","5","","41","LAYOUT OF MAIN COMPONENTS","m_be_0084.pdf",
"","5","STEERING PAD SWITCH","42","DESCRIPTION","m_be_0085.pdf",
"","5","BLOCKING SYSTEM","43","DESCRIPTION","m_be_0086.pdf",
"","5","","43","SYSTEM DIAGRAM ","m_be_0087.pdf",
"","5","","43","LAYOUT OF MAIN COMPONENTS","m_be_0087.pdf",
"","5","","43","FUNCTION OF MAIN COMPONENTS","m_be_0088.pdf",
"","5","","43","SYSTEM OPERATION","m_be_0088.pdf",
"","5","","43","DIAGNOSIS","m_be_0088.pdf",
"APPENDIX","6","MAJOR TECHNICAL SPECIFICATIONS","44","MAJOR TECHNICAL SPECIFICATIONS","m_ap_0002.pdf"
);

